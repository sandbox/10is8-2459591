Drupal.behaviors.traffic_advisorBehavior = {
  attach: function (context, settings) {
	function initialize() {
	  var myLatlng = new google.maps.LatLng(settings.traffic_advisor.lat_textfield, settings.traffic_advisor.lon_textfield);
	   var mapOptions = {
	    zoom: 11,
	    center: myLatlng,
	    disableDefaultUI: true
	  }
	  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

	  var trafficLayer = new google.maps.TrafficLayer();
	  trafficLayer.setMap(map);
	}

	google.maps.event.addDomListener(window, 'load', initialize);
  }
};

