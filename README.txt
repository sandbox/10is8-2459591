CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------
Traffic Advisor generates a Google Map with Traffic overlay block that can be 
placed anywhere on site to show current traffic for location.


 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/10is8/2459591


 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2459591?categories=All

REQUIREMENTS
------------
No special requirements. 

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * Go to your blocks and find Traffic Advisor block, click configure. 


   - Enter lat & long for location you wish to display traffic for.
     You can use http://www.latlong.net/ to find the coordinates.  


   - Select Region where you want block to appear and any other default block
     settings. 

TROUBLESHOOTING
---------------
 * If the block does not display, check the following:


   - Is correct Region selected and does template render the region? 


   - Is block restricted by path or role? 


MAINTAINERS
-----------
Current maintainers:
 * Pedja Grujic (pgrujic) - https://www.drupal.org/u/pgrujic
 * Akira Koide (ak55) - https://www.drupal.org/u/ak55
 